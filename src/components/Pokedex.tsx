import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Details from "./Details";
import Pokemons from "./Pokemons";
import FoundPokemon from "./FoundPokemon";
import "./Pokedex.css";

const Pokedex: React.FC = () => {
  const [allPokemons, setAllPokemons] = useState<any | null>([]);
  const [loadMore, setLoadMore] = useState(
    "https://pokeapi.co/api/v2/pokemon?limit=20"
  );
  const [search, setSearch] = useState<string>("");
  const [searchedPokemon, setSearchedPokemon] = useState({});

  const getSearchedPokemon: Function = async () => {
    const res = await fetch(
      `https://pokeapi.co/api/v2/pokemon/${search.toLowerCase()}`
    );
    const data = await res.json();

    console.log("Found data", data);

    setSearchedPokemon(data);
  };

  const getAllPokemons: Function = async () => {
    const res = await fetch(loadMore);
    const data = await res.json();

    console.log("dataFetch", data);

    setLoadMore(data.next);

    const createPokemonObject: Function = (results: any[]) => {
      results.forEach(async (pokemon) => {
        const res = await fetch(
          `https://pokeapi.co/api/v2/pokemon/${pokemon.name}`
        );
        const data = await res.json();

        setAllPokemons((currentList: any[]) => [...currentList, data]);
      });

      allPokemons.sort((a: { id: number }, b: { id: number }) => a.id - b.id);
    };
    createPokemonObject(data.results);
  };

  useEffect(() => {
    getAllPokemons();
    // eslint-disable-next-line
  }, []);

  const handleChange: Function = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value.length >= 2) setSearch(e.target.value);

    console.log(search);
  };

  const handleSearch: Function = () => {
    getSearchedPokemon();
  };

  return (
    <Router>
      <Switch>
        {allPokemons.map((details: any) => (
          <Route
            key={`route_${details.name}`}
            path={`/pokemons/${details.name}`}
          >
            <Details {...details} />
          </Route>
        ))}
        <Route path="/">
          <div className="wrapper">
            <h1 className="pokedex-title">Pokedex</h1>
            <input
              className="pokedex-search"
              type="text"
              onChange={(e) => handleChange(e)}
              placeholder="Type here to search pokemon!"
            />
            <input
              className="pokedex-search-button"
              value="Search"
              type="button"
              onClick={() => handleSearch()}
            />
            {search !== "" && searchedPokemon.hasOwnProperty('name') ? (
              <FoundPokemon {...searchedPokemon} />
            ) : (
              false
            )}
            <div className="pokedex-container">
              {allPokemons.map((details: any) => (
                <Pokemons key={details.name} {...details} />
              ))}
            </div>
            <input
              className="pokedex-loadmore-button"
              value="Load more"
              type="button"
              onClick={() => getAllPokemons()}
            />
          </div>
        </Route>
      </Switch>
    </Router>
  );
};

export default Pokedex;
