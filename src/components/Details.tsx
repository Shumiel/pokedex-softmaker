import pickColor from "./ColorCheck";

const Details: React.FC = (props: any) => {
  return (
    <div>
      <div className="pokedex-item">
        <p>{"#" + props.id}</p>
        <p>{props.name}</p>
        <img
          className="pokedex-item-img"
          alt={props.name + "_img"}
          src={props.sprites.other.dream_world.front_default}
        ></img>
        <p>XP: {props.base_experience}</p>
        <p>Height: {props.height}</p>
        <p>Weight: {props.weight}</p>
        <p>Skills:</p>
        {props.abilities.map((ele: any) => (
          <p key={`skill_${ele.ability.name}`}>{ele.ability.name}</p>
        ))}
        <p
          style={{
            backgroundColor: pickColor(props.types[0].type.name),
            borderRadius: "1rem",
          }}
        >
          {props.types[0].type.name}
        </p>
      </div>
    </div>
  );
};

export default Details;
