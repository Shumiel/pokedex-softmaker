const pickColor = (type) => {
    let color = "";

    if (type === "grass") {
      color = "green";
    } else if (type === "water") {
      color = "blue";
    } else if (type === "bug") {
      color = "green";
    } else if (type === "normal") {
      color = "BurlyWood";
    } else if (type === "poison") {
      color = "purple";
    } else if (type === "fairy") {
      color = "pink";
    } else if (type === "fire") {
      color = "red";
    } else if (type === "ground") {
      color = "brown";
    } else if (type === "fighting") {
      color = "grey";
    } else if (type === "electric") {
      color = "yellow";
    } else if (type === "psychic") {
      color = "RebeccaPurple";
    } else if (type === "rock") {
      color = "SlateGray";
    } else if (type === "ghost") {
      color = "black";
    }

    return color;
  };


export default pickColor;