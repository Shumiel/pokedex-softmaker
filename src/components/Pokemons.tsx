import { Link } from "react-router-dom";
import pickColor from "./ColorCheck";

const Pokemons: React.FC = (props: any) => {
  return (
    <Link to={`/pokemons/${props.name}`}>
      <div className="pokedex-item card-anim">
        <p>{"#" + props.id}</p>
        <p>{props.name}</p>
        <img
          className="pokedex-item-img"
          alt={props.name + "_img"}
          src={props.sprites.other.dream_world.front_default}
        ></img>
        <p
          style={{
            backgroundColor: pickColor(props.types[0].type.name),
            borderRadius: "1rem",
          }}
        >
          {props.types[0].type.name}
        </p>
      </div>
    </Link>
  );
};

export default Pokemons;
